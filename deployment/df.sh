#!/bin/bash

cd /var/www/html/magento245
bin/magento maintenance:enable
git pull
composer install
php n98-magerun2.phar generation:flush
bin/magento setup:upgrade
bin/magento setup:di:compile
bin/magento setup:static-content:deploy -j 4 en_US
composer install # to apply patches
bin/magento maintenance:disable
